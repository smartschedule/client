import * as core from "@angular/core";
import { Observable } from "rxjs";

import { DayConstraintDto, AgeConstraintDto, AddressConstraintDto, GenderConstraintDto, NeighborhoodDto, GenderDto, AgeDto } from "../../models"
import { HttpClient , HttpHeaders } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import { DayDto } from '../../models/dto/DayDto';
import { AppSettings } from "src/app/AppSettings";
import { Injectable } from "@angular/core";
const ApiBaseRoute = AppSettings.API_ENDPOINT + "Constraint/";
@Injectable({
  providedIn: 'root'
})

export class ConstraintService {
    dayConstraint: DayConstraintDto[];
    addressConstraint: AddressConstraintDto[];
    ageConstraint: AgeConstraintDto[];
    genderConstraint: GenderConstraintDto[];
    days: DayDto[];
    ages: AgeDto[];
    neighborhoods: NeighborhoodDto[];
    genders: GenderDto[];




    constructor(private http: HttpClient) {
       
    }

    getAddressConstraints(teacherId: string): Observable<AddressConstraintDto[]> {
      return this.http.get<AddressConstraintDto[]>(ApiBaseRoute + "GetAddressConstraint?teacherId=" + teacherId);
  }

  getDayConstraints(teacherId: string): Observable<DayConstraintDto[]> {
        return this.http.get<DayConstraintDto[]>(ApiBaseRoute + "GetDayConstraint?teacherId=" + teacherId);
}
getAgeConstraints(teacherId: string): Observable<AgeConstraintDto[]> {
  return this.http.get<AgeConstraintDto[]>(ApiBaseRoute + "GetAgeConstraint?teacherId=" + teacherId);
}
getGenderConstraints(teacherId: string): Observable<GenderConstraintDto[]> {
  return this.http.get<GenderConstraintDto[]>(ApiBaseRoute + "GetGenderConstraint?teacherId=" + teacherId);
}
    
public getAllNeighborhoods(): Observable<NeighborhoodDto[]> {
  return this.http.get<NeighborhoodDto[]>(ApiBaseRoute + "GetAllNeighborhoods");
}
   
    public getAllGenders(): Observable<GenderDto[]> {
      return this.http.get<GenderDto[]>(ApiBaseRoute + "GetAllGenders");

    }
    public getAllAges(): Observable<AgeDto[]> {
        return this.http.get<AgeDto[]>(ApiBaseRoute + "GetAllAges");
      }
    public getAllDays(): Observable<DayDto[]> {
        return this.http.get<DayDto[]>(ApiBaseRoute + "GetAllDays");
    }
    //////////////////////////////////////////////////////////////////////////////////////////////save

    setDayConstraints(dayCode: string[], teacherId: string): Observable<boolean> {
      return this.http.post<boolean>(ApiBaseRoute + "SaveDaysConstraint?teacherId=" + teacherId, dayCode);
  }

    
    setAddressConstraints(teacherId: string, address: AddressConstraintDto[]): Observable<boolean> {
        return null;
    }
    setAgeConstraints(teacherId: string, ages: AgeConstraintDto[]): Observable<boolean> {
        return null;

    }
    setGenderConstraints(teacherId: string, genders: GenderConstraintDto[]): Observable<boolean> {
        return null;

    }

}