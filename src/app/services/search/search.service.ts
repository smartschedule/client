import * as core from "@angular/core";
import { Observable } from 'rxjs';
import { TeacherDto } from '../../Models/dto/TeacherDto';
import { Injectable } from "@angular/core";
import { AppSettings } from "src/app/AppSettings";
import { HttpClient , HttpHeaders } from '@angular/common/http';
const ApiBaseRoute = AppSettings.API_ENDPOINT + "Search/";


@Injectable({
    providedIn: 'root'
  })
  export class SearchService  {
    constructor(private http: HttpClient){
       

    }
    teacherId:number=0;
    public searchExcanging(firstDate:Date,secondDate:Date):Observable<TeacherDto[]>{
       console.log(firstDate.toLocaleDateString());
       console.log(ApiBaseRoute + "GetExschanging?teacherId=" + this.teacherId +"&firstDate="+firstDate.toLocaleDateString()+"&secondDate="+secondDate.toLocaleDateString());
        return this.http.get<TeacherDto[]>(ApiBaseRoute + "GetExschanging?teacherId=" + this.teacherId +"&firstDate="+firstDate.toLocaleDateString()+"&secondDate="+secondDate.toLocaleDateString());
    }
} 