import { Component, OnInit } from '@angular/core';
import { UserService } from '../../core/user.service';
import { AuthService } from '../../core/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirebaseUserModel } from '../../core/user.model';
import { TeacherDto } from 'src/app/models';
import { LoginService } from 'src/app/services/Auth/login.service';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit{

  user: FirebaseUserModel = new FirebaseUserModel();
  profileForm: FormGroup;
  // userDB:   User= new User() ;

  //  newUser: User= new User();
   isLogin:string;
   flag=0;
   u=null;
  constructor(
    public userService: UserService,
    public authService: AuthService,
    public loginService: LoginService,

    private route: ActivatedRoute,
    private location : Location,
    private r: Router,
    private fb: FormBuilder,
    
  ) {
this.isLogin=localStorage.getItem('idUser');

  }
teacher:TeacherDto= new TeacherDto();
    ngOnInit(): void {
    
   this.loginService.getTeacherByEmail().subscribe(
    teacher => { 
                this.teacher=teacher
                localStorage.setItem('teacher', JSON.stringify(this.teacher));

                console.log("teacher",this.teacher)

                 
                  err => console.error('Observer got an error: ' + err)
      
              });
 

      this.route.data.subscribe(routeData => {
      let data = routeData['data'];
      if (data) {
        this.user = data;
        this.createForm(this.user.name);
        localStorage.setItem('profil', JSON.stringify(this.user.image));
        localStorage.setItem('name', JSON.stringify(this.user.name));
        localStorage.setItem('provider', JSON.stringify(this.user.provider));
        this.r.navigateByUrl("/home");

this.refresh();
      }
    })
  }
  refresh(): void {
    if(JSON.parse(localStorage.getItem('flag'))!="1"){
      localStorage.setItem('flag', JSON.stringify("1"));
      window.location.reload();

  }
}

  createForm(name) {
    this.profileForm = this.fb.group({
      name: [name, Validators.required ]
    });
  }

  save(value){
  
    //this.saveUserInDB();

    this.user.name=value.name;
    this.userService.updateCurrentUser(value)
    .then(res => {
      console.log("value",res);
     // window.location.reload();

    }, err => console.log(err))
   // 

  }
//   saveUserInDB() {
   
//     console.log(this.defultEvent);
    
//   //this.userDB.eventDefault=this.defultEvent;
//   //console.log("userDB ",this.userDB);

    
//    this.myService.putUserToDB(this.defultEvent ).subscribe(data => 
//      { this.newUser.Id = data,   
      
//      console.log("10 ",this.defultEvent); 
//      this.u="";
//      window.location.reload();
//     }
//      , err => console.log("error to save DB") );
     

// }
logout(){
  localStorage.removeItem('idUser');
  localStorage.removeItem('profil');
  localStorage.removeItem('name');
  localStorage.removeItem('provider');
  localStorage.removeItem('flag');
  this.isLogin=null;
 
  this.authService.doLogout()
  .then((res) => {
    this.r.navigate(["/login"])

  }, (error) => {
    console.log("Logout error", error);
    this.r.navigate(["/login"])
  });
}
}
