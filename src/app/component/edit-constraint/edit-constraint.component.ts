import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ConstraintService } from '../../services/Constraint/constraint.service';
import { Router } from '@angular/router';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { DayDto } from '../../Models/dto/DayDto';
import { AgeDto, NeighborhoodDto, GenderDto, DayConstraintDto, AgeConstraintDto, AddressConstraintDto, GenderConstraintDto } from '../../Models';
import { ChangeDetectionStrategy } from '@angular/core';


@Component({
  selector: 'edit-constraint',
  //changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './edit-constraint.component.html',
  styleUrls: ['./edit-constraint.component.css']
})
export class EditConstraintComponent implements OnInit {

  @Output() onFinishEdit = new EventEmitter();




  days: DayDto[];
  age: AgeDto[];
  neighborhood: NeighborhoodDto[];
  gender: GenderDto[];
  daySelectedValues: string[]=[];
  ageSelectedValues: string[]=[];
  genderSelectedValues: string[]=[];
  addressSelectedValues: string[]=[];
  statusSaveConstraint:number=0;
  techerId;
  // selectedValues2:string[]=["1","2","4"];
  constructor(private router: Router, private constraintService: ConstraintService) {
    let x=JSON.parse(localStorage.getItem("teacher"));
    this.techerId=x.Id;

  }

  ngOnInit() {

    this.constraintService.getAllDays().subscribe(
      (data: DayDto[]) => {
        this.constraintService.days = data;
        this.days = data;
        console.log("days", this.days);
        this.checkDaysValue();
      },
      fail => alert("שגיאה בשרת"));

    this.constraintService.getAllAges().subscribe(
      (data: AgeDto[]) => {
        this.constraintService.ages = data;
        this.age = data;
        console.log("age", this.age);
        this.checkAgesValue();
      },
      fail => alert("שגיאה בשרת"));

    this.constraintService.getAllNeighborhoods().subscribe(
      (data: NeighborhoodDto[]) => {
        this.constraintService.neighborhoods = data;
        this.neighborhood = data;
        console.log("neighborhood", this.neighborhood);
        this.checkAddressesValue();
      },
      fail => alert("שגיאה בשרת"));

    this.constraintService.getAllGenders().subscribe(
      (data: GenderDto[]) => {
        this.constraintService.genders = data;
        this.gender = data;
        console.log("gender", this.gender);
        this.checkGendersValue();
      },
      fail => alert("שגיאה בשרת"));


  }

  saveConstraint() {
    //this.onFinishEdit.emit(false);
    this.constraintService.setDayConstraints(this.daySelectedValues,this.techerId).subscribe(
      (data:boolean)=>{
        alert("ok!!!!");
        // if (data==true)
        //     this.statusSaveConstraint++;
       
    });

    if( this.statusSaveConstraint==4)
    alert("הנתונים נקלטו בהצלחה");
    this.router.navigateByUrl("/home");
  }
  returnHome(){
    this.router.navigateByUrl("/home");

  }
  checkDaysValue() {
    if (this.constraintService.dayConstraint == undefined) {
      this.constraintService.getDayConstraints(this.techerId).subscribe(
        (data: DayConstraintDto[]) => {
          this.constraintService.dayConstraint = data;
          for (let dayConstraint of this.constraintService.dayConstraint) {
            this.daySelectedValues.push(dayConstraint.DayCode.toString());
            //break;
          }

        },
        fail => alert("שגיאה בשרת"));
    }
    else {
      for (let dayConstraint of this.constraintService.dayConstraint) {
        this.daySelectedValues.push(dayConstraint.DayCode.toString());
        // break;
      }

    }
  }
    checkAgesValue() {
      if (this.constraintService.ageConstraint == undefined) {
        this.constraintService.getAgeConstraints(this.techerId).subscribe(
          (data: AgeConstraintDto[]) => {
            this.constraintService.ageConstraint = data;
            for (let ageConstraint of this.constraintService.ageConstraint) {
              this.ageSelectedValues.push(ageConstraint.AgeCode.toString());
            }

          },
          fail => alert("שגיאה בשרת"));
      }
      else {
        for (let ageConstraint of this.constraintService.ageConstraint) {
          this.ageSelectedValues.push(ageConstraint.AgeCode.toString());
        }

      }
    }
    
      checkAddressesValue() {
        if (this.constraintService.addressConstraint == undefined) {
          this.constraintService.getAddressConstraints(this.techerId).subscribe(
            (data: AddressConstraintDto[]) => {
              this.constraintService.addressConstraint = data;
              for (let addressConstraint of this.constraintService.addressConstraint) {
                this.addressSelectedValues.push(addressConstraint.NeighborhoodCode.toString());
              }
  
            },
            fail => alert("שגיאה בשרת"));
        }
        else {
          for (let addressConstraint of this.constraintService.addressConstraint) {
            this.addressSelectedValues.push(addressConstraint.NeighborhoodCode.toString());
          }
  
        }
      }

      checkGendersValue() {
        if (this.constraintService.genderConstraint == undefined) {
          this.constraintService.getGenderConstraints(this.techerId).subscribe(
            (data:GenderConstraintDto[]) => {
              this.constraintService.genderConstraint = data;
              for (let genderConstraint of this.constraintService.genderConstraint) {
                this.genderSelectedValues.push(genderConstraint.GenderCode.toString());
              }
  
            },
            fail => alert("שגיאה בשרת"));
        }
        else {
          for (let genderConstraint of this.constraintService.genderConstraint) {
            this.genderSelectedValues.push(genderConstraint.GenderCode.toString());
          }
  
        }
      }
      
    }
