import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CalendarModule } from 'primeng/calendar';
import { ButtonModule } from 'primeng/button';
import { SearchService } from '../../services/search/search.service';
import { TeacherDto } from '../../Models/dto/TeacherDto';


@Component({
  selector: 'search',
  //changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css', '../../../styles/w3.css']
})
export class SearchComponent implements OnInit {
  firstDate: Date;
  secondDate: Date;
  exchangingTeachers: TeacherDto[];



  constructor(private searchService: SearchService) { }
  x: boolean;
  ngOnInit() {
  }
  searchExchanging() {

    if (this.firstDate.getTime() <= Date.now() || this.secondDate.getTime() <= Date.now()) {
      alert("אי אפשר לבחור תאריך שחלף")
      return;
    }

    if (this.firstDate.getDay() == 6 || this.firstDate.getDay() == 7 || this.secondDate.getDate() == 6 || this.secondDate.getDate() == 7) {
      alert("אי אפשר לבחור יום שישי או שבת")
      return;
    }
    this.x = true;
    this.searchService.searchExcanging(this.firstDate, this.secondDate).subscribe(
      (data: TeacherDto[]) => {
        this.exchangingTeachers = data;
        console.log("exchangingTeachers", this.exchangingTeachers);
        this.x = false
      },
      fail => {
        alert("teacher search שגיאה בשרת");
        this.x = false
    });
      
   
  }
}
