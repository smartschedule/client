
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TeacherDto, TeacherScheduleDto, AddressConstraintDto, DayConstraintDto, AgeConstraintDto, GenderConstraintDto } from 'src/app/models';
import { ScheduleService } from 'src/app/services/schdule/schdule.service';
import { ConstraintService } from 'src/app/services/Constraint/constraint.service';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  teacherSchedule: TeacherScheduleDto[];
  AddressConstraint: AddressConstraintDto[];
  DayConstraint: DayConstraintDto[];
  AgeConstraint: AgeConstraintDto[];
  GenderConstraint: GenderConstraintDto[];
  teacher:TeacherDto;
  constructor( private router: Router,private scheduleService:ScheduleService, private constraintService:ConstraintService) { 
  this.teacher=JSON.parse(localStorage.getItem("teacher"));
  }

  ngOnInit() {

      //   this.scheduleService.getSchedule(new Date().getFullYear().toString(), this.teacher.Id).subscribe(
      // (data: TeacherScheduleDto[]) => {
      //   this.teacherSchedule = data;
      //   console.log("schedule", data);
      //   console.log("schedule", this.teacherSchedule);
      // });
      //fail => alert("teacherSchedule שגיאה בשרת"));

    this.constraintService.getAddressConstraints(this.teacher.Id).subscribe(
      (data: AddressConstraintDto[]) => {
        this.constraintService.addressConstraint = data;
        this.AddressConstraint = data;
        console.log("address constraint", this.AddressConstraint);
      },
      fail => alert("שגיאה בשרת"));

    this.constraintService.getDayConstraints(this.teacher.Id).subscribe(
      (data: DayConstraintDto[]) => {

        this.constraintService.dayConstraint = data;
        this.DayConstraint = data;
        console.log("day constraint", this.DayConstraint);
      },
      fail => alert("שגיאה בשרת"));

    this.constraintService.getAgeConstraints(this.teacher.Id).subscribe(
      (data: AgeConstraintDto[]) => {
        this.constraintService.ageConstraint = data;
        this.AgeConstraint = data;
        console.log("age constraint", this.AgeConstraint);
      },
      fail => alert("שגיאה בשרת"));

    this.constraintService.getGenderConstraints(this.teacher.Id).subscribe(
      (data: GenderConstraintDto[]) => {
        this.constraintService.genderConstraint = data;
        this.GenderConstraint = data;
        console.log("gender constraint", this.GenderConstraint);
      },
      fail => alert("שגיאה בשרת"));
  }
  
  editConstraint(){
    this.router.navigateByUrl("/edit-constraint");
  }
}
