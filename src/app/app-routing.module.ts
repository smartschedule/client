
import { LoginComponent } from './component/login/login.component';
import { UserComponent } from './component/user/user.component';
import { RegisterComponent } from './component/register/register.component';
import { UserResolver } from './component/user/user.resolver';
import { AuthGuard } from './core/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './component/home/home.component';
import { SearchComponent } from './component/search/search.component';
import { ExchangeComponent } from './component/exchange/exchange.component';
import { EditConstraintComponent } from './component/edit-constraint/edit-constraint.component';




export const rootRouterConfig: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent, canActivate: [AuthGuard] },
  { path: 'register', component: RegisterComponent, canActivate: [AuthGuard] },
  { path: 'user', component: UserComponent,  resolve: { data: UserResolver}},
  {path:'home', component:HomeComponent,},
  {path:'search',component:SearchComponent},
  {path:'exchange',component:ExchangeComponent,},
  {path:'edit-constraint',component:EditConstraintComponent,}
];