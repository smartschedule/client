export interface UserDto {
    IsAuthorized: boolean;
    UserId: number;
    UserName: string;
    ErrorMessage: string;
}