export class NeighborhoodDto {
    NeighborhoodCode: number
    NeighborhoodName: string
    CityCode: number
}