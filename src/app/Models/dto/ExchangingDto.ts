export class ExchangingDto {
    Id: number
    Year: number
    ClassId: number
    SourceDate: Date
    TecherId: number
    SubstituteTeacherId: number
    DestinationDate: Date
}