export class ClassDto {

    Id: number
    OrganizationCode: number
    Name: string
    NeighborhoodCode: number
    Street: string
    BuildingNumber: number
    AgeCode: number
    GenderCode: number
}
