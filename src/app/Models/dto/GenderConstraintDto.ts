export class GenderConstraintDto {

    Id: number
    TeacherId: number
    GenderCode: number
    GenderName: string
}