import { Component } from '@angular/core';
import { AuthService } from './core/auth.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']

})
export class AppComponent {
  title = 'smartSchedulClient';
  iduser:string;
  profil;
  name;
  provider;
  isLogin=null;
  constructor(
    public authService: AuthService,
    private location : Location,
    private r: Router,
    
  ) {


  }
  ngOnInit() {
 

    this.isLogin=JSON.parse(localStorage.getItem('idUser'));
    console.log("this.isLogin",this.isLogin);
    this.iduser= JSON.parse(localStorage.getItem('idUser'));
    this.profil= JSON.parse(localStorage.getItem('profil'));
    this.name= JSON.parse(localStorage.getItem('name'));
    this.provider= JSON.parse(localStorage.getItem('provider'));

  }
  logout(){
    localStorage.removeItem('idUser');
    localStorage.removeItem('profil');
    localStorage.removeItem('name');
    localStorage.removeItem('provider');
    localStorage.removeItem('flag');
    localStorage.removeItem('email');
    localStorage.removeItem('teacher');

  localStorage.clear();
//localStorage=null;
    this.isLogin=null;
    this.iduser= null;
    this.profil= null;
    this.provider=null;
    this.authService.doLogout()
    .then((res) => {
//this.location.back();
      this.r.navigate(["/login"])

    }, (error) => {
      console.log("Logout error", error);
      this.r.navigate(["/login"])
    });
  }
}