import { Injectable } from "@angular/core";
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
// import { User } from "../models/User";
import { Observable } from 'rxjs';
import {AppSettings} from '../AppSettings';
// import { UsersService } from "../service/user.service";
const  ApiBaseRoute = AppSettings.API_ENDPOINT;

@Injectable()
export class AuthService {
  //  שמירת ה uid של המשתמש 
//  public static idUser : String;
dataSource ;

  constructor(
   public afAuth: AngularFireAuth,    

 ){}

  // doFacebookLogin(){
  //   return new Promise<any>((resolve, reject) => {
  //     let provider = new firebase.auth.FacebookAuthProvider();
  //     this.afAuth.auth
  //     .signInWithPopup(provider)
  //     .then(res => {
  //       resolve(res);
  //     }, err => {
  //       console.log(err);
  //       reject(err);
  //     })
  //   })
  // }

  // doTwitterLogin(){
  //   return new Promise<any>((resolve, reject) => {
  //     let provider = new firebase.auth.TwitterAuthProvider();
  //     this.afAuth.auth
  //     .signInWithPopup(provider)
  //     .then(res => {
  //       resolve(res);
  //     }, err => {
  //       console.log(err);
  //       reject(err);
  //     })
  //   })
  // }

  doGoogleLogin(){
    return new Promise<any>((resolve, reject) => {
      let provider = new firebase.auth.GoogleAuthProvider();
      provider.addScope('profile');
      provider.addScope('email');
      provider.setCustomParameters({
        prompt: 'select_account'
      });
      this.afAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        console.log("xcv" ,res);
     //   console.log( "res",res.user.uid);
    // UsersService.idUser=  res.user.uid;
    localStorage.setItem('idUser', JSON.stringify(res.user.uid));
    localStorage.setItem('email', JSON.stringify(res.user.email));

        resolve(res);
      
      }, err => {
        console.log(err);
        reject(err);
      })
    })
  }

  doRegister(value){
    return new Promise<any>((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
      .then(res => {
        localStorage.setItem('idUser', JSON.stringify(res.user.uid));
        resolve(res);
      }, err => reject(err))
    })
  }

  doLogin(value){
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(value.email, value.password)
      .then(res => {
        localStorage.setItem('idUser', JSON.stringify(res.user.uid));


        resolve(res);
      }, err => reject(err))
    })
  }

  doLogout(){
    return new Promise((resolve, reject) => {
      if(firebase.auth().currentUser){
        this.afAuth.auth.signOut()
        localStorage.removeItem('idUser');
        // this.myService.user=null;
        resolve();
      }
      else{
        reject();
      }
    });
  }


}
